package me.terturl.Civ5;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import metrics.Metrics;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Civilization extends JavaPlugin
{
  Logger log = Logger.getLogger("Minecraft");
  public static Permission perm = null;
  public static Economy econ = null;
  public static Chat chat = null;
  
  private boolean setupPermissions()
  {
    RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
    if (permissionProvider != null) {
      perm = (Permission)permissionProvider.getProvider();
    }
    return perm != null;
  }

  private boolean setupChat()
  {
    RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(Chat.class);
    if (chatProvider != null) {
      chat = (Chat)chatProvider.getProvider();
    }

    return chat != null;
  }

  private boolean setupEconomy()
  {
    RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
    if (economyProvider != null) {
      econ = (Economy)economyProvider.getProvider();
    }

    return econ != null;
  }

  public void onEnable() {
	  
    this.log.info(String.format("[%s] Enabled Version %s", new Object[] { getDescription().getName(), getDescription().getVersion() }));
    this.log.log(Level.FINE, "Please report all bugs on the Bukkit page!");

    getServer().getPluginManager().registerEvents(new CantPickUp(), this);
    try
    {
      Metrics metrics = new Metrics(this);
      metrics.start();
    } catch (IOException e) {
      e.printStackTrace();
    }

    setupChat();
    setupPermissions();
    setupEconomy();

    if (!setupChat()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
    if (!setupEconomy()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
    if (!setupPermissions()) {
      this.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
      getServer().getPluginManager().disablePlugin(this);
      return;
    }
  }

  public void onDisable() {
    this.log.info(String.format("[%s] Disabled Version %s", new Object[] { getDescription().getName(), getDescription().getVersion() }));
  }

  public boolean onCommand(final CommandSender sender, Command command, String label, String[] args) {
    Player p = (Player)sender;

    if ((label.equalsIgnoreCase("civ")) && 
      (args.length == 0)) {
      p.sendMessage(ChatColor.GREEN + "------Help------");
      p.sendMessage(ChatColor.AQUA + "/research" + ChatColor.GREEN + " Displays research help");
      p.sendMessage(ChatColor.AQUA + "/city" + ChatColor.GREEN + " Displays city help");
      p.sendMessage(ChatColor.AQUA + "/build" + ChatColor.GREEN + " Displays building help");
      p.sendMessage(ChatColor.GREEN + "----------------");
    }
    if (label.equalsIgnoreCase("research")) {
      if (args.length == 0) {
        p.sendMessage(ChatColor.GREEN + "------Help------");
        p.sendMessage(ChatColor.AQUA + "Ancient" + ChatColor.GREEN + " Shows all Ancient era tech");
        p.sendMessage(ChatColor.AQUA + "Classical" + ChatColor.GREEN + " Shas all Classical era tech");
        p.sendMessage(ChatColor.AQUA + "Medieval" + ChatColor.GREEN + " Shas all Medieval era tech");
        p.sendMessage(ChatColor.AQUA + "Renaissance" + ChatColor.GREEN + " Shas all Renaissance era tech");
        p.sendMessage(ChatColor.AQUA + "Industrial" + ChatColor.GREEN + " Shas all Industrial era tech");
        p.sendMessage(ChatColor.AQUA + "Modern" + ChatColor.GREEN + " Shas all Modern era tech");
        p.sendMessage(ChatColor.AQUA + "Future" + ChatColor.GREEN + " Shas all Future era tech");
        p.sendMessage(ChatColor.AQUA + "{tech name}" + ChatColor.GREEN + " Researches tech");
        p.sendMessage(ChatColor.GREEN + "------Help------");
      }
      
      if (args.length == 1) {
        if (args[0].equalsIgnoreCase("ancient")) {
          p.sendMessage(ChatColor.GREEN + "---Ancient Era Tech---");
          sender.sendMessage(ChatColor.AQUA + "Agriculture" + ChatColor.GOLD + " $2000" + ChatColor.LIGHT_PURPLE + " Basic Tech");
          sender.sendMessage(ChatColor.AQUA + "Pottery" + ChatColor.GOLD + " $2500" + ChatColor.LIGHT_PURPLE + " requires Agriculture");
          sender.sendMessage(ChatColor.AQUA + "Animal-Husbandary" + ChatColor.GOLD + " $2500" + ChatColor.LIGHT_PURPLE + " requires Agriculture");
          sender.sendMessage(ChatColor.AQUA + "Archery" + ChatColor.GOLD + " $2500" + ChatColor.LIGHT_PURPLE + " requires Agriculture");
          sender.sendMessage(ChatColor.AQUA + "Mining" + ChatColor.GOLD + " $2500" + ChatColor.LIGHT_PURPLE + " requires Agriculture");
          sender.sendMessage(ChatColor.AQUA + "Sailing" + ChatColor.GOLD + " $3500" + ChatColor.LIGHT_PURPLE + " requires Pottery");
          sender.sendMessage(ChatColor.AQUA + "Calender" + ChatColor.GOLD + " $3500" + ChatColor.LIGHT_PURPLE + " requires Pottery");
          sender.sendMessage(ChatColor.AQUA + "Trapping" + ChatColor.GOLD + " $4000" + ChatColor.LIGHT_PURPLE + " requires Animal Husbandar");
          sender.sendMessage(ChatColor.AQUA + "The-Wheel" + ChatColor.GOLD + " $4000" + ChatColor.LIGHT_PURPLE + " requires Animal Husbandary");
          sender.sendMessage(ChatColor.AQUA + "Masonary" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Mining");
          sender.sendMessage(ChatColor.AQUA + "Bronze-Working" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Mining");
          p.sendMessage(ChatColor.GREEN + "---Ancient Era Tech---");
        }
        if (args[0].equalsIgnoreCase("agriculture")) {
          if (perm.has(p, "civ.tech.ancient.agriculture")) {
            p.sendMessage(ChatColor.RED + "You have already researched tech!");
          }
          else if (econ.has(p.getName(), 2000.0D)) {
            p.sendMessage(ChatColor.GOLD + "This tech will take 5 minutes to research");
            econ.withdrawPlayer(p.getName(), 2000.0D);
            Bukkit.getScheduler().runTaskLater(this, new Runnable()
            {
              public void run() {
                Player p = (Player)sender;
                p.sendMessage(ChatColor.GOLD + "You have finished researching agriculture!");
                perm.playerAdd(p, "civ.tech.ancient.agriculture");
              }
            }
            , 6000L); } else {
            p.sendMessage(ChatColor.RED + "You need at least $2000 to research this tech!");
          }
        }
        if (args[0].equalsIgnoreCase("pottery")) {
          if (perm.has(p, "civ.tech.ancient.pottery")) {
            p.sendMessage(ChatColor.RED + "You have already researched this tech!");
          }
          else if (econ.has(p.getName(), 2500.0D)) {
            if (perm.has(p, "civ.tech.ancient.agriculture")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 7 minutes to research");
              econ.withdrawPlayer(p.getName(), 25000.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching potterye!");
                  perm.playerAdd(p, "civ.tech.ancient.pottery");
                }
              }
              , 8400L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research agriculuture!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You must have at least $2500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("animal-husbandary")) {
          if (perm.has(p, "civ.tech.ancient.AnimalHusbandary")) {
            p.sendMessage(ChatColor.RED + "You have already researched this tech!");
          }
          else if (econ.has(p.getName(), 2500.0D)) {
            if (perm.has(p, "civ.tech.ancient.agriculture")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 7 minutes to research");
              econ.withdrawPlayer(p.getName(), 2500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Animal Husbandary!");
                  perm.playerAdd(p, "civ.tech.ancient.AnimalHusbandary");
                }
              }
              , 8400L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research agriculture!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You must have at least $2500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("archery")) {
          if (perm.has(p, "civ.tech.ancient.archery")) {
            p.sendMessage(ChatColor.RED + "You have already research this tech");
          }
          else if (econ.has(p.getName(), 2500.0D)) {
            if (perm.has(p, "civ.tech.ancient.agriculture")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 7 minutes to research");
              econ.withdrawPlayer(p.getName(), 2500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Archery!");
                  perm.playerAdd(p, "civ.tech.ancient.archery");
                }
              }
              , 8400L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research agriculture");
            }
          }
          else p.sendMessage(ChatColor.RED + "You must have at least $2500 to research this tech");

        }

        if (args[0].equalsIgnoreCase("mining")) {
          if (perm.has(p, "civ.tech.ancient.mining")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 2500.0D)) {
            if (perm.has(p, "civ.tech.ancient.agriculture")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 7 minutes to research");
              econ.withdrawPlayer(p.getName(), 2500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Mining!");
                  perm.playerAdd(p, "civ.tech.ancient.mining");
                }
              }
              , 8400L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research agriculture!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $2500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("sailing")) {
          if (perm.has(p, "civ.tech.ancient.sailing")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 3500.0D)) {
            if (perm.has(p, "civ.tech.ancient.pottery")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 10 minutes to research");
              econ.withdrawPlayer(p.getName(), 3500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Sailing!");
                  perm.playerAdd(p, "civ.tech.ancient.sailing");
                }
              }
              , 12000L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research pottery!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $3500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("calender")) {
          if (perm.has(p, "civ.tech.ancient.calender")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 3500.0D)) {
            if (perm.has(p, "civ.tech.ancient.potteru")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 10 minutes to research");
              econ.withdrawPlayer(p.getName(), 3500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Calender!");
                  perm.playerAdd(p, "civ.tech.ancient.calender");
                }
              }
              , 12000L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research pottery!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $3500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("trapping")) {
          if (perm.has(p, "civ.tech.ancient.trapping")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 4000.0D)) {
            if (perm.has(p, "civ.tech.ancient.AnimalHusbandary")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 10 minutes to research");
              econ.withdrawPlayer(p.getName(), 4000.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Trapping!");
                 perm.playerAdd(p, "civ.tech.ancient.trapping");
                }
              }
              , 12000L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research Animal-Husbandary!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $4000 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("the-wheel")) {
          if (perm.has(p, "civ.tech.ancient.TheWheel")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 4000.0D)) {
            if (perm.has(p, "civ.tech.ancient.AnimalHusbandary")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 10 minutes to research");
              econ.withdrawPlayer(p.getName(), 4000.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching The Wheel!");
                  perm.playerAdd(p, "civ.tech.ancient.TheWheel");
                }
              }
              , 12000L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research Animal-Husbandary!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $4000 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("masonary")) {
          if (perm.has(p, "civ.tech.ancient.masonary")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 5500.0D)) {
            if (perm.has(p, "civ.tech.ancient.mining")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 12 minutes to research");
              econ.withdrawPlayer(p.getName(), 5500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Masonary!");
                  perm.playerAdd(p, "civ.tech.ancient.masonary");
                }
              }
              , 14400L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research Mining!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $5500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("bronze-working")) {
          if (perm.has(p, "civ.tech.ancient.BronzeWorking")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 5500.0D)) {
            if (perm.has(p, "civ.tech.ancient.mining")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 14 minutes to research");
              econ.withdrawPlayer(p.getName(), 5500.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Bronze Working!");
                  perm.playerAdd(p, "civ.tech.ancient.BronzeWorking");
                }
              }
              , 16800L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research mining!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $5500 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("classical")) {
          if ((perm.has(p, "civ.tech.ancient.agriculture")) && (perm.has(p, "civ.tech.ancient.pottery")) && (perm.has(p, "civ.tech.ancient.AnimalHusbandary")) && (perm.has(p, "civ.tech.ancient.mining")) && (perm.has(p, "civ.tech.ancient.archery")) && (perm.has(p, "civ.tech.ancient.sailing")) && (perm.has(p, "civ.tech.ancient.calender")) && (perm.has(p, "civ.tech.ancient.trapping")) && (perm.has(p, "civ.tech.ancient.TheWheel")) && (perm.has(p, "civ.tech.ancient.masonary")) && (perm.has(p, "civ.tech.ancient.BronzeWorking"))) {
            if(!perm.has(p, "civ.tech.rank.classical")) {
            	Bukkit.broadcastMessage(ChatColor.GOLD + p.getName() + " is now in the" + ChatColor.RED + " Classical Era" + ChatColor.GOLD + "!");
            	chat.setPlayerPrefix(p, ChatColor.BLACK + "[" + ChatColor.GOLD + "Classical" + ChatColor.BLACK + "]");
            	perm.playerAdd(p, "civ.tech.rank.classical");
            }
        	sender.sendMessage(ChatColor.BLUE + "+----Classical Era Tech----+");
            sender.sendMessage(ChatColor.AQUA + "Optics" + ChatColor.GOLD + " $6000" + ChatColor.LIGHT_PURPLE + " requires Sailing");
            sender.sendMessage(ChatColor.AQUA + "Philosophy" + ChatColor.GOLD + " $6000" + ChatColor.LIGHT_PURPLE + " Basic Tech");
            sender.sendMessage(ChatColor.AQUA + "HorseBack-Riding" + ChatColor.GOLD + " $6250" + ChatColor.LIGHT_PURPLE + " requires The Wheel");
            sender.sendMessage(ChatColor.AQUA + "Mathematics" + ChatColor.GOLD + " $6250" + ChatColor.LIGHT_PURPLE + " requires The Wheel");
            sender.sendMessage(ChatColor.AQUA + "Construction" + ChatColor.GOLD + " $6500" + ChatColor.LIGHT_PURPLE + " requires Masonary");
            sender.sendMessage(ChatColor.AQUA + "Iron Working" + ChatColor.GOLD + " $6500" + ChatColor.LIGHT_PURPLE + " requires Bronze Working");
            sender.sendMessage(ChatColor.BLUE + "+----Classical Era Tech----+");
          } else {
            p.sendMessage(ChatColor.RED + "You need to research all the ancient era tech to see this!");
          }
        }
        if (args[0].equalsIgnoreCase("optics")) {
          if (perm.has(p, "civ.tech.classical.optics")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 6000.0D)) {
            if (perm.has(p, "civ.tech.ancient.sailing")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 14 minutes to research");
              econ.withdrawPlayer(p.getName(), 6000.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Optics!");
                  perm.playerAdd(p, "civ.tech.classical.optics");
                }
              }
              , 16800L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research sailing!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $6000 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("philosophy")) {
          if (perm.has(p, "civ.tech.classical.philosophy")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 6000.0D)) {
            if (perm.has(p, "civ.tech.ancient.sailing")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 14 minutes to research");
              econ.withdrawPlayer(p.getName(), 6000.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Philosophy!");
                  perm.playerAdd(p, "civ.tech.classical.philosophy");
                }
              }
              , 16800L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research sailing!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $6000 to research this tech!");

        }

        if (args[0].equalsIgnoreCase("HorseBack-Riding")) {
          if (perm.has(p, "civ.tech.classical.HorsebackRiding")) {
            p.sendMessage(ChatColor.RED + "You have already research that tech!");
          }
          else if (econ.has(p.getName(), 6250.0D)) {
            if (perm.has(p, "civ.tech.ancient.TheWheel")) {
              p.sendMessage(ChatColor.GOLD + "This tech will take 15 minutes to research");
              econ.withdrawPlayer(p.getName(), 6250.0D);
              Bukkit.getScheduler().runTaskLater(this, new Runnable()
              {
                public void run() {
                  Player p = (Player)sender;
                  p.sendMessage(ChatColor.GOLD + "You have finished researching Horseback-Riding!");
                  perm.playerAdd(p, "civ.tech.classical.HorsebackRiding");
                }}, 18000L);
            } else {
              p.sendMessage(ChatColor.RED + "You must first research The-Wheel!");
            }
          }
          else p.sendMessage(ChatColor.RED + "You need at least $6250 to research this tech!");
        }
        if (args[0].equalsIgnoreCase("mathematics")) {
            if (perm.has(p, "civ.tech.classical.mathematics")) {
              p.sendMessage(ChatColor.RED + "You have already research that tech!");
            }
            else if (econ.has(p.getName(), 6250.0D)) {
              if (perm.has(p, "civ.tech.ancient.TheWheel")) {
                p.sendMessage(ChatColor.GOLD + "This tech will take 15 minutes to research");
                econ.withdrawPlayer(p.getName(), 6250.0D);
                Bukkit.getScheduler().runTaskLater(this, new Runnable()
                {
                  public void run() {
                    Player p = (Player)sender;
                    p.sendMessage(ChatColor.GOLD + "You have finished researching Mathematics!");
                    perm.playerAdd(p, "civ.tech.classical.mathematics");
                  }}, 18000L);
              } else {
                p.sendMessage(ChatColor.RED + "You must first research The-Wheel!");
              }
            }
            else p.sendMessage(ChatColor.RED + "You need at least $6250 to research this tech!");
          }
        if (args[0].equalsIgnoreCase("Construction")) {
            if (perm.has(p, "civ.tech.ancient.masonary")) {
              p.sendMessage(ChatColor.RED + "You have already research that tech!");
            }
            else if (econ.has(p.getName(), 6500.0D)) {
              if (perm.has(p, "civ.tech.classical.construction")) {
                p.sendMessage(ChatColor.GOLD + "This tech will take 18 minutes to research");
                econ.withdrawPlayer(p.getName(), 6500.0D);
                Bukkit.getScheduler().runTaskLater(this, new Runnable()
                {
                  public void run() {
                    Player p = (Player)sender;
                    p.sendMessage(ChatColor.GOLD + "You have finished researching Construction!");
                    perm.playerAdd(p, "civ.tech.classical.construction");
                  }}, 21600L);
              } else {
                p.sendMessage(ChatColor.RED + "You must first research Masonary!");
              }
            }
            else p.sendMessage(ChatColor.RED + "You need at least $6500 to research this tech!");
          }

        if (args[0].equalsIgnoreCase("medieval")) {
        	if(!perm.has(p, "civ.tech.rank.medieval")) {
        		Bukkit.broadcastMessage(ChatColor.GOLD + p.getName() + " is now in the" + ChatColor.RED + " Medieval Era" + ChatColor.GOLD + "!");
            	chat.setPlayerPrefix(p, ChatColor.BLACK + "[" + ChatColor.GOLD + "Classical" + ChatColor.BLACK + "]");
            	perm.playerAdd(p, "civ.tech.rank.classical");
        	}
          sender.sendMessage(ChatColor.DARK_GRAY + "+----Medieval Era Tech----+");
          sender.sendMessage(ChatColor.AQUA + "Theology" + ChatColor.GOLD + " $7000" + ChatColor.LIGHT_PURPLE + " requires Philosophy");
          sender.sendMessage(ChatColor.AQUA + "Civil-Service" + ChatColor.GOLD + " $7000" + ChatColor.LIGHT_PURPLE + " requires Philosophy");
          sender.sendMessage(ChatColor.AQUA + "Currency" + ChatColor.GOLD + " $7000" + ChatColor.LIGHT_PURPLE + " requires Mathemtics");
          sender.sendMessage(ChatColor.AQUA + "Engineering" + ChatColor.GOLD + " $3250" + ChatColor.LIGHT_PURPLE + " requires Construction");
          sender.sendMessage(ChatColor.AQUA + "Metal-Casting" + ChatColor.GOLD + " $3000" + ChatColor.LIGHT_PURPLE + " requires Iron Working");
          sender.sendMessage(ChatColor.AQUA + "Compass" + ChatColor.GOLD + " $3250" + ChatColor.LIGHT_PURPLE + " requires Optics");
          sender.sendMessage(ChatColor.AQUA + "Education" + ChatColor.GOLD + " $3250" + ChatColor.LIGHT_PURPLE + " requires Theology");
          sender.sendMessage(ChatColor.AQUA + "Chivalry" + ChatColor.GOLD + " $3400" + ChatColor.LIGHT_PURPLE + " requires HorseBack Riding");
          sender.sendMessage(ChatColor.AQUA + "Machinery" + ChatColor.GOLD + " $3500" + ChatColor.LIGHT_PURPLE + " requires Engineering");
          sender.sendMessage(ChatColor.AQUA + "Physics" + ChatColor.GOLD + " $3500" + ChatColor.LIGHT_PURPLE + " requires Engineering");
          sender.sendMessage(ChatColor.AQUA + "Steel" + ChatColor.GOLD + " $3600" + ChatColor.LIGHT_PURPLE + " requires Metal Casting");
          sender.sendMessage(ChatColor.DARK_GRAY + "+----Medieval Era Tech----+");
        }
        
        if (args[0].equalsIgnoreCase("Theology")) {
            if (perm.has(p, "civ.tech.medieval.theology")) {
              p.sendMessage(ChatColor.RED + "You have already research that tech!");
            }
            else if (econ.has(p.getName(), 7000.0D)) {
              if (perm.has(p, "civ.tech.classical.philosophy")) {
                p.sendMessage(ChatColor.GOLD + "This tech will take 20 minutes to research");
                econ.withdrawPlayer(p.getName(), 7000.0D);
                Bukkit.getScheduler().runTaskLater(this, new Runnable()
                {
                  public void run() {
                    Player p = (Player)sender;
                    p.sendMessage(ChatColor.GOLD + "You have finished researching Theology!");
                    perm.playerAdd(p, "civ.tech.medieval.theology");
                  }}, 24000L);
              } else {
                p.sendMessage(ChatColor.RED + "You must first research Masonary!");
              }
            }
            else p.sendMessage(ChatColor.RED + "You need at least $7000 to research this tech!");
        }
        
        if (args[0].equalsIgnoreCase("civil-service")) {
            if (perm.has(p, "civ.tech.medieval.CivilService")) {
              p.sendMessage(ChatColor.RED + "You have already research that tech!");
            }
            else if (econ.has(p.getName(), 7000.0D)) {
              if (perm.has(p, "civ.tech.classical.philosophy")) {
                p.sendMessage(ChatColor.GOLD + "This tech will take 20 minutes to research");
                econ.withdrawPlayer(p.getName(), 7000.0D);
                Bukkit.getScheduler().runTaskLater(this, new Runnable()
                {
                  public void run() {
                    Player p = (Player)sender;
                    p.sendMessage(ChatColor.GOLD + "You have finished researching Civil-Service!!");
                    perm.playerAdd(p, "civ.tech.medieval.CivilService");
                  }}, 24000L);
              } else {
                p.sendMessage(ChatColor.RED + "You must first research Philosohpy!");
              }
            }
            else p.sendMessage(ChatColor.RED + "You need at least $7000 to research this tech!");
        }
        
        if (args[0].equalsIgnoreCase("currency")) {
            if (perm.has(p, "civ.tech.medieval.currency")) {
                p.sendMessage(ChatColor.RED + "You have already researched this tech!");
            }
            else if (econ.has(p.getName(), 7000.0D) {
                if (perm.has(p, "civ.tech.classical.mathematics") {
                    p.sendMessage(ChatColor.GOLD + "This tech will take 20 minutes to research");
                    econ.withdrawPlayer(p.getName(), 7000.0D);
                    Bukkit.getScheduler().runTaskLater(this, new Runnable()
                    {
                        public void run() {
                            Player p = (Player)sender;
                            p.sendMessage(ChatColor.GOLD + "You have finished researching Currency!");
                            perm.playerAdd(p, "civ.tech.medieval.currency");
                        }
                    }, 24000L);
                } else {
                    p.sendMessage(ChatColor.RED + "You must first research Mathematics!");
                }
            } else {
                p.sendMessage(ChatColor.RED + "You need at least $7000 to research this tech!");
            }
        }

        if ((args[0].equalsIgnoreCase("renaissance")) && 
          (p.isOp())) {
          sender.sendMessage(ChatColor.RED + "+----Renaissance Era Tech----+");
          sender.sendMessage(ChatColor.AQUA + "Astromonmy" + ChatColor.GOLD + " $3800" + ChatColor.LIGHT_PURPLE + " requires Education");
          sender.sendMessage(ChatColor.AQUA + "Acoustics" + ChatColor.GOLD + " $3850" + ChatColor.LIGHT_PURPLE + " requires Chivalry");
          sender.sendMessage(ChatColor.AQUA + "Banking" + ChatColor.GOLD + " $3850" + ChatColor.LIGHT_PURPLE + " requires Chivalry");
          sender.sendMessage(ChatColor.AQUA + "Printing-Press" + ChatColor.GOLD + " $3850" + ChatColor.LIGHT_PURPLE + " requires Machinery");
          sender.sendMessage(ChatColor.AQUA + "Gunpowder" + ChatColor.GOLD + " $3900" + ChatColor.LIGHT_PURPLE + " requires Machinery");
          sender.sendMessage(ChatColor.AQUA + "Navigation" + ChatColor.GOLD + " $4000" + ChatColor.LIGHT_PURPLE + " requires Astronomy");
          sender.sendMessage(ChatColor.AQUA + "Economics" + ChatColor.GOLD + " $4200" + ChatColor.LIGHT_PURPLE + " requires Banking");
          sender.sendMessage(ChatColor.AQUA + "Chemistry" + ChatColor.GOLD + " $4500" + ChatColor.LIGHT_PURPLE + " requires Gunpowder");
          sender.sendMessage(ChatColor.AQUA + "Metallurgy" + ChatColor.GOLD + " $4500" + ChatColor.LIGHT_PURPLE + " requires Gunpowder");
          sender.sendMessage(ChatColor.AQUA + "Archaeology" + ChatColor.GOLD + " $4700" + ChatColor.LIGHT_PURPLE + " requires Navigation");
          sender.sendMessage(ChatColor.AQUA + "Scientific-Theory" + ChatColor.GOLD + " $4750" + ChatColor.LIGHT_PURPLE + " requires Acoustics");
          sender.sendMessage(ChatColor.AQUA + "Military-Science" + ChatColor.GOLD + " $4800" + ChatColor.LIGHT_PURPLE + " requires Economics");
          sender.sendMessage(ChatColor.AQUA + "Fertilizer" + ChatColor.GOLD + " $4800" + ChatColor.LIGHT_PURPLE + " requires Chemistry");
          sender.sendMessage(ChatColor.AQUA + "Rifling" + ChatColor.GOLD + " $5000" + ChatColor.LIGHT_PURPLE + " requires Metallurgy");
          sender.sendMessage(ChatColor.RED + "+----Renaissance Era Tech----+");
        }

        if ((args[0].equalsIgnoreCase("Industrial")) && 
          (p.isOp())) {
          sender.sendMessage(ChatColor.GREEN + "+----Idustrial Era Tech----+");
          sender.sendMessage(ChatColor.AQUA + "Biology" + ChatColor.GOLD + " $5200" + ChatColor.LIGHT_PURPLE + " requires Scientific Theory");
          sender.sendMessage(ChatColor.AQUA + "Steam-Power" + ChatColor.GOLD + " $5200" + ChatColor.LIGHT_PURPLE + " requires Military Science");
          sender.sendMessage(ChatColor.AQUA + "Electricity" + ChatColor.GOLD + " $5250" + ChatColor.LIGHT_PURPLE + " requires Biology");
          sender.sendMessage(ChatColor.AQUA + "Replaceable-Parts" + ChatColor.GOLD + " $5300" + ChatColor.LIGHT_PURPLE + " requires Steam Power");
          sender.sendMessage(ChatColor.AQUA + "Railroad" + ChatColor.GOLD + " $5300" + ChatColor.LIGHT_PURPLE + " requires Steam Power");
          sender.sendMessage(ChatColor.AQUA + "Dynamite" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Rifling");
          sender.sendMessage(ChatColor.AQUA + "Refrigeration" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Electricity");
          sender.sendMessage(ChatColor.AQUA + "Telegraph" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Electricity");
          sender.sendMessage(ChatColor.AQUA + "Radio" + ChatColor.GOLD + " $5500" + ChatColor.LIGHT_PURPLE + " requires Electricity");
          sender.sendMessage(ChatColor.AQUA + "Flight" + ChatColor.GOLD + " $5600" + ChatColor.LIGHT_PURPLE + " requires Replaceable Parts");
          sender.sendMessage(ChatColor.AQUA + "Combustion" + ChatColor.GOLD + " $5800" + ChatColor.LIGHT_PURPLE + " requires Dynamite");
          sender.sendMessage(ChatColor.GREEN + "+----Idustrial Era Tech----+");
        }

        if ((args[0].equalsIgnoreCase("Modern")) && 
          (p.isOp())) {
          sender.sendMessage(ChatColor.DARK_GRAY + "+----Modern Era Tech----+");
          sender.sendMessage(ChatColor.AQUA + "Plastics" + ChatColor.GOLD + " $6000" + ChatColor.LIGHT_PURPLE + " requires Refrigeration");
          sender.sendMessage(ChatColor.AQUA + "Penicillin" + ChatColor.GOLD + " $6000" + ChatColor.LIGHT_PURPLE + " requires Refrigeration");
          sender.sendMessage(ChatColor.AQUA + "Electronics" + ChatColor.GOLD + " $6200" + ChatColor.LIGHT_PURPLE + " requires Telegraph");
          sender.sendMessage(ChatColor.AQUA + "Mass-Media" + ChatColor.GOLD + " $6200" + ChatColor.LIGHT_PURPLE + " requires Radio");
          sender.sendMessage(ChatColor.AQUA + "Radar" + ChatColor.GOLD + " $6400" + ChatColor.LIGHT_PURPLE + " requires Flight");
          sender.sendMessage(ChatColor.AQUA + "Atomic-Theory" + ChatColor.GOLD + " $6400" + ChatColor.LIGHT_PURPLE + " requires Combustion");
          sender.sendMessage(ChatColor.AQUA + "Penicillin" + ChatColor.GOLD + " $6450" + ChatColor.LIGHT_PURPLE + " requires Plastics");
          sender.sendMessage(ChatColor.AQUA + "Computers" + ChatColor.GOLD + " $6600" + ChatColor.LIGHT_PURPLE + " requires Electronics");
          sender.sendMessage(ChatColor.AQUA + "Rocketry" + ChatColor.GOLD + " $6600" + ChatColor.LIGHT_PURPLE + " requires Radar");
          sender.sendMessage(ChatColor.AQUA + "Lasers" + ChatColor.GOLD + " $6600" + ChatColor.LIGHT_PURPLE + " requires Combustion");
          sender.sendMessage(ChatColor.AQUA + "Nuclear-Fission" + ChatColor.GOLD + " $6800" + ChatColor.LIGHT_PURPLE + " requires Atomic Theory");
          sender.sendMessage(ChatColor.AQUA + "Globilization" + ChatColor.GOLD + " $6800" + ChatColor.LIGHT_PURPLE + " requires Ecology");
          sender.sendMessage(ChatColor.AQUA + "Robotics" + ChatColor.GOLD + " $7200" + ChatColor.LIGHT_PURPLE + " requires Computers");
          sender.sendMessage(ChatColor.AQUA + "Satellites" + ChatColor.GOLD + " $7400" + ChatColor.LIGHT_PURPLE + " requires Rocketry");
          sender.sendMessage(ChatColor.AQUA + "Stealth" + ChatColor.GOLD + " $7600" + ChatColor.LIGHT_PURPLE + " requires Lasers");
          sender.sendMessage(ChatColor.AQUA + "Advanced-Ballistics" + ChatColor.GOLD + " $7800" + ChatColor.LIGHT_PURPLE + " requires Nuclear Fission");
          sender.sendMessage(ChatColor.DARK_GRAY + "+----Modern Era Tech----+");
        }

        if ((args[0].equalsIgnoreCase("Future")) && 
          (p.isOp())) {
          sender.sendMessage(ChatColor.RED + "+----Future Era Tech----+");
          sender.sendMessage(ChatColor.AQUA + "Particle-Physics" + ChatColor.GOLD + " $8500" + ChatColor.LIGHT_PURPLE + " requires Globalization, Robotics, Satellites");
          sender.sendMessage(ChatColor.AQUA + "Nuclear-Fusion" + ChatColor.GOLD + " $8800" + ChatColor.LIGHT_PURPLE + " requires Satellites, Stealth, Advanced Ballistics");
          sender.sendMessage(ChatColor.AQUA + "Nanotechnology" + ChatColor.GOLD + " $9500" + ChatColor.LIGHT_PURPLE + " requires Particle Physics");
          sender.sendMessage(ChatColor.AQUA + "Future-Tech" + ChatColor.GOLD + " $10000" + ChatColor.LIGHT_PURPLE + " requires Nuclear Fusion, Nanotechnology");
          sender.sendMessage(ChatColor.RED + "+----Future Era Tech----+");
        }
      }
    }

    return false;
  }
}