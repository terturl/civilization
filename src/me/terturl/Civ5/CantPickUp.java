package me.terturl.Civ5;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class CantPickUp
  implements Listener
{
  public Civilization plugin;
  
  public void test(PlayerPickupItemEvent event)
  {
    Player p = event.getPlayer();

    if ((event.getItem().equals(Material.BOW)) && 
      (!Civilization.perm.has(p, "civ.tech.ancient.archery"))) {
      event.setCancelled(true);
    } else {
    	event.setCancelled(false);
    }
    
    if((event.getItem().equals(Material.STONE_AXE)) && event.getItem().equals(Material.STONE_HOE) && event.getItem().equals(Material.STONE_PICKAXE) && event.getItem().equals(Material.STONE_SPADE) && event.getItem().equals(Material.STONE_SWORD) && !Civilization.perm.has(p, "civ.tech.ancient.masonary")) {
    	event.setCancelled(true);
    } else {
    	event.setCancelled(false);
    }
  }
}